package com.example.paginator.repository;

import androidx.annotation.Nullable;

import java.util.List;

public interface PagedRepository<T> {
    List<T> find(@Nullable Integer offset, int sizePage);
}
