package com.example.paginator.repository;

import androidx.annotation.Nullable;

import com.example.paginator.model.Dog;

import java.util.ArrayList;
import java.util.List;

public class DogRepository implements PagedRepository {
    @Override
    public List find(@Nullable Integer offset, int untilPage) {
        if (offset > getDogList().size()) offset = getDogList().size();
        if (untilPage > getDogList().size()) untilPage = getDogList().size();
        return getDogList().subList(offset, untilPage);
    }

    public static List<Dog> getDogList() {
        List<Dog> dogList = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            dogList.add(new Dog(String.valueOf(i), "perro " + i));
        }
        return dogList;
    }

}
