package com.example.paginator.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.paginator.R;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    DogViewModel dogViewModel;
    DogsAdapter dogsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        initAdapters();
        initObservers();
    }

    private void bindViews() {
        recyclerView = findViewById(R.id.recycler);
    }

    private void initObservers() {
            getViewModel().getListMutableLiveData().observe(this, dogsAdapter::submitList);
    }

    private DogViewModel getViewModel() {
        if (dogViewModel == null) {
            dogViewModel = new ViewModelProvider(this).get(DogViewModel.class);
        }
        return dogViewModel;
    }

    private void initAdapters(){
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        dogsAdapter = new DogsAdapter();
        recyclerView.setAdapter(dogsAdapter);
    }
}