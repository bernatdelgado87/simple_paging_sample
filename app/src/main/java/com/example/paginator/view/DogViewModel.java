package com.example.paginator.view;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.example.paginator.dataSource.DataSourceFactory;
import com.example.paginator.model.Dog;
import com.example.paginator.repository.DogRepository;

public class DogViewModel extends ViewModel {
    DogRepository dogRepository = new DogRepository();
    LiveData<PagedList<Dog>> listMutableLiveData;
    private DataSourceFactory dataSourceFactory;

    public DogViewModel() {
        initPaginator();
    }

    private void initPaginator() {
        dataSourceFactory = new DataSourceFactory(dogRepository);

        PagedList.Config myPagingConfig = new PagedList.Config.Builder()
                .setPageSize(20) // les q demanem a cada scroll
                .setPrefetchDistance(3) //preloaditems
                .setMaxSize(100) // maxim numero ditems q es guarden en memoria
                .setEnablePlaceholders(true) // placehoders pels items q s'estan carregant
                .setInitialLoadSizeHint(3) // inicials
                .build();

        listMutableLiveData = new LivePagedListBuilder<>(dataSourceFactory, myPagingConfig).build();
    }

    public LiveData<PagedList<Dog>> getListMutableLiveData() {
        return listMutableLiveData;
    }
}
