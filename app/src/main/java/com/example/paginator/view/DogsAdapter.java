package com.example.paginator.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.paginator.R;
import com.example.paginator.model.Dog;

public class DogsAdapter  extends PagedListAdapter<Dog, DogsAdapter.DogViewHolder> {

    protected DogsAdapter() {
        super(Dog.DIFF_CALLBACK);
    }

    @NonNull
    @Override
    public DogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view, parent, false);
        DogViewHolder viewHolder = new DogViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DogsAdapter.DogViewHolder holder, int position) {
            holder.dogTitle.setText(getItem(position).getId() + getItem(position).getName());
    }

    public static class DogViewHolder extends RecyclerView.ViewHolder {
        TextView dogTitle;

        DogViewHolder(View itemView) {
            super(itemView);
            dogTitle = itemView.findViewById(R.id.categoriaTexto);
        }
    }
}
