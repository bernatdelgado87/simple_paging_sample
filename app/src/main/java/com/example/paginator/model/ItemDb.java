package com.example.paginator.model;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

public class ItemDb {
    String id;

    public ItemDb(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return super.equals(obj);
    }

    public static DiffUtil.ItemCallback<ItemDb> DIFF_CALLBACK = new DiffUtil.ItemCallback<ItemDb>() {
        // Concert details may have changed if reloaded from the database,
        // but ID is fixed.
        @Override
        public boolean areItemsTheSame(ItemDb oldItem, ItemDb newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(ItemDb oldItem, ItemDb newItem) {
            return oldItem.equals(newItem);
        }
    };
}
