package com.example.paginator.model;

import androidx.recyclerview.widget.DiffUtil;

public class Dog extends ItemDb{
    String name;

    public Dog(String code, String name) {
        super(code);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static DiffUtil.ItemCallback<Dog> DIFF_CALLBACK = new DiffUtil.ItemCallback<Dog>() {
        // Concert details may have changed if reloaded from the database,
        // but ID is fixed.
        @Override
        public boolean areItemsTheSame(Dog oldItem, Dog newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(Dog oldItem, Dog newItem) {
            return oldItem.equals(newItem);
        }
    };
}
