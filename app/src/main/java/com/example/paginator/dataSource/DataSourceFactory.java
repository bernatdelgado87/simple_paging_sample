package com.example.paginator.dataSource;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import com.example.paginator.repository.PagedRepository;

public class DataSourceFactory<T> extends DataSource.Factory {
    private final MutableLiveData<DogDataSource<T>> sourceLiveData = new MutableLiveData<>();
    private final PagedRepository<T> repository;


    public DataSourceFactory(PagedRepository<T> repository) {
        this.repository = repository;
    }

    @NonNull
    @Override
    public DogDataSource<T> create() {
        DogDataSource<T> dataSource = new DogDataSource<T>(repository);
        sourceLiveData.postValue(dataSource);
        return dataSource;
    }


    //todo implement on every refresh of data
    public void refresh() {
        sourceLiveData.getValue().invalidate();
    }
}
