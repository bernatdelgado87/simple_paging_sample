package com.example.paginator.dataSource;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.example.paginator.repository.PagedRepository;

import java.util.List;

public class DogDataSource<E> extends PageKeyedDataSource<Integer, E> {
    private PagedRepository<E> repository;

    public DogDataSource(PagedRepository<E> repository) {
        this.repository = repository;
    }


    @Override
    public void loadInitial(@NonNull  PageKeyedDataSource.LoadInitialParams<Integer> params, @NonNull  PageKeyedDataSource.LoadInitialCallback<Integer, E> callback) {
        int size = params.requestedLoadSize;
        List<E> results = repository.find(0, size);
        callback.onResult(results, 0, size+1);
    }

    @Override
    public void loadBefore(@NonNull  PageKeyedDataSource.LoadParams<Integer> params, @NonNull  PageKeyedDataSource.LoadCallback<Integer, E> callback) {

    }

    @Override
    public void loadAfter(@NonNull  PageKeyedDataSource.LoadParams<Integer> params, @NonNull  PageKeyedDataSource.LoadCallback<Integer, E> callback) {
        Log.d("bernat", "loadAfter");

        int offset = params.key;
        int size = params.requestedLoadSize;
        List<E> results = repository.find(offset, size + offset);
        callback.onResult(results, size+offset);
}
}
